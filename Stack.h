#ifndef DYNCHARSTACK_H
#define DYNCHARSTACK_H

class DynCharStack{

  private:
    struct StackNode {

      char value;
      StackNode* next;
    };

    StackNode* top;

  public:
    DynCharStack()
    { top = NULL; }

    ~DynCharStack();

    void push(char);
    void pop(char &);
    bool isEmpty();

};

#endif


