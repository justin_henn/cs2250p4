#include <iostream>
#include "Stack.h"

using namespace std;

DynCharStack::~DynCharStack() {

  StackNode* nodePtr, *nextNode;

  nodePtr = top;

  while (nodePtr = NULL) {
    nextNode = nodePtr->next;
    delete nodePtr;
    nodePtr = nextNode;
  }
}

void DynCharStack::push(char c) {

  StackNode* newNode;

  newNode = new StackNode;
  newNode->value = c;

  if (isEmpty()) {
    
    top = newNode;
    newNode->next = NULL;
  }

  else {

    newNode->next = top;
    top = newNode;
  }
}

void DynCharStack::pop(char& c) {

  StackNode* temp;

  if (isEmpty()) {

    cout << "The stack is empty. \n";
  }
  else {

    c = top->value;
    temp = top->next;
    delete top;
    top = temp;
  }
}

bool DynCharStack::isEmpty() {

  bool status;

  if (!top)
    status = true;
  else
    status = false;

  return status;
}
