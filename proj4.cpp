#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "Stack.h"
#include <fstream>


using namespace std;

int main() {

  fstream inputFile; //creates a file stream
  inputFile.open("test.txt"); //opens test.txt
  string input; //string to take the sentence if delimiters don't match
  int count, tmp; //counter for where the program is at in the file

  DynCharStack stack; //creates a stack
  char ch; //char to hold the char from the file

  while(!inputFile.eof()) { //loop that stops when at eof


    inputFile.get(ch); //gets the char from the file
    stack.push(ch); //pushes the char on the stack
    count++; //increments the counter
    if(ch == '{') { //checks to see if there is a braces
      tmp = count; //makes temp the counter where the sentence starts

      while (ch != '}' && ch != ')' && ch != ']') { //loops that adds to stack until it encounters ending delimter
	inputFile.get(ch);
	stack.push(ch);
	count++;
	//cout << ch << endl;
      }
      if(ch == ']') { //checks to see if it is a bracket end delimter
      
	inputFile.seekg(tmp, ios::beg); //changes where to read the file to the beginning of the sentence
	getline(inputFile, input, ']'); // makes the input string the sentence
	cout << input << endl; //outputs the string
      }
      else if (ch == ')') { //checks to see if it a parantheses end delimter

	inputFile.seekg(tmp, ios::beg); //changes where to read the file to the beginning of the sentence
	getline(inputFile, input, ')'); //makes the input string the sentence
	cout << input << endl; //outputs the string
      }
     
      /*else
	//cout << "they match" << endl;
	inputFile.seekg(tmp, ios::beg);
	getline(inputFile, input, '}');
	cout << input << endl;*/
    
    }
  }
 
    
  inputFile.close(); //closes the file
    
   /* char yes = 'H';

  stack.push(yes);
  stack.pop(test);
  cout << test << endl;*/


  return 0;

}
